use<Soporte_Riel_Optico.scad>
use<Bobina_Hemholzt.scad>

diametro = 180;
radio_pared = 3;
extrude_largo = 50;

rotate([90,0,0]) soporte_bobina();
rotate([90, 0, 0]) translate([0,diametro/2+radio_pared,extrude_largo/2]) rotate([0, -270, 0]) bobina();
