diametro_bobina = 180;
wall = 10;
radio_pared = 3;
extrude_largo = 50;
alto_soporte = 20;
ancho_soporte = 70;
cubo_base_bru = diametro_bobina/2-ancho_soporte ;
alto_base_bru = 3;

altura_brujula = 10;
radio_brujula = 30;

escalax=1.5;
escalay=0.9;
escalaz=0.9;

rotate([0,0,0]){
    translate([0,0,diametro_bobina/2-alto_base_bru-2]){
        difference(){
            cube([2.5*radio_brujula,2.5*radio_brujula,4], center=true);
            translate([0,0,1.6]){
                cube([2*radio_brujula,1.25,1],center=true);
                cube([1.25,2*radio_brujula,1],center=true);
                translate([2,(2/2)*radio_brujula-5]){
                    linear_extrude([0,0,0,2])
                        text("N",size=5);
                }         
            }
        }
    }
    translate([0,0,(diametro_bobina/2-alto_base_bru-4)/2]){
        difference(){
            cube([cubo_base_bru,cubo_base_bru,diametro_bobina/2-alto_base_bru-4],center=true);
            cube([cubo_base_bru-radio_pared,cubo_base_bru-radio_pared,diametro_bobina/2-alto_base_bru-4],center=true);
            
        }
    }
}