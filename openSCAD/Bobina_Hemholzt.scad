/******************************************************************
*                                                                 *
* Bobinas de Helmholtz: Bobina propiamente dicha                  *
*                                                                 *
*                                                                 *
* (c) Pablo Cremades, Abril 2023                                  *
* Released under the CERN Open Hardware License                   *
*                                                                 *
******************************************************************/

include<bend.scad>

diametro = 180;
wall = 10;

$fn=80;

module bobina(){
  difference(){
    color("blue") cylinder(d=diametro, h=wall*2, center=true); //Coil main body
    rotate_extrude(convexity = 10)
      translate([diametro/2, 0, 0]) circle(d=wall); //The grove
    cylinder(d=diametro-wall*2, h=wall*3, center=true); //Central hole

    //Arrows indicating wiring direction
    for(alpha=[-90, 0, 90])
      color("red") rotate([0, 0, alpha+8]) translate([0, diametro/2-wall/3, 10.1]) import("arrow.stl", convexity=3);
  }
}

bobina();
