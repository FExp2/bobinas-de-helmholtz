// Módulo de soporte de deslizamiento para riel de aluminio
//
// Copyright (c) 2021 Maximiliano Ventura
//
// The MIT License (MIT)
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without restriction,
// including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
// and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do
// so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial
// portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
// NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
// SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


/*
Esquema: vista frontal

--------------------- 
        ||||
      --------
      \______/

*/

module Holder_Riel_Mod(altura=9){
        $fn = 50;

        //x = ancho
        //y = largo
        //z = alto
        // protuberancia: pieza inferior
        // central: conector base-protuberancia

        base_riel=2.5; //Grosor de la base superior
        ancho_riel=40; //Ancho total pieza (eje x)
        pared_riel=1.5; //Tamaño de los cortes intermedios
        ancho_central=11; //Grosor de la pieza central
        largo_central=8.6; //Largo total de la pieza
        ancho_protuberancia=7.1; //Grosor en el eje y de la protuberancia
        largo_protuberancia=16;
        radio_1=3.6; //Radio de los cilindros de la protuberancia

        translate([0, base_riel,0]) rotate([0, 0, -90]){
                cube([base_riel, ancho_riel, altura]); //base

                translate([0, 15.7, 0])
                        cube([ancho_central, largo_central, altura]); //prisma conector


                translate([base_riel + pared_riel, (ancho_riel - largo_protuberancia)/2, 0])
                        cube([ancho_protuberancia - radio_1, largo_protuberancia, altura]); //prisma central

                translate([base_riel + pared_riel + ancho_protuberancia/2, (ancho_riel - largo_central)/2 + largo_central, 0])
                        cylinder(altura, radio_1, radio_1);

                translate([base_riel + pared_riel + ancho_protuberancia/2, (ancho_riel - largo_central)/2, 0])
                        cylinder(altura, radio_1, radio_1);
        }
}
