include<bend.scad>

diametro = 180;

module arrow(){
  rotate([0, 90, -90]) cylindric_bend(dimensions=[5, 30, 5], radius=diametro/2, nsteps=8)
  rotate([90, 0, 90]) linear_extrude(2) import("arrow.svg", dpi=96);
}

arrow();
