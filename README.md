# Bobinas de Helmholtz

Diseño de bobinas de Helmholtz para experimentos de magnetismo.

<img src="img/foto_1.jpg" alt="drawing" height="300"/>
<img src="img/foto_2.jpg" alt="drawing" height="300"/>
<img src="img/foto_3.jpg" alt="drawing" height="300"/>
<img src="img/render.png" alt="drawing" height="300"/>

## Características

- Diámetro: 180mm
- Soporte para riel óptico con borneras de conexión

## Fuente de Corriente
<img src="img/regulador_corriente.png" alt="drawing" height="300"/>

- Regulación: 20mA a 200mA
- Tensión de alimentación: 10V a 30V

## License

This project is open-source and is released under the CERN open hardware license.  There is not yet any accompanying software, but if that changes please consider it GPL.  Any documentation contained in this project is licensed CC-BY International 4.0.  We are working on selling kits through OpenFlexure Industries, though we're not yet taking orders - however this will stay a fully open-source project.
